@extends('layouts.master')

<!-- Page Title -->
@section('title','Home')
<!-- /Page Title -->

<!-- Page Specific Stylesheets -->
@section('stylesheets')
    <!-- Datatables -->
    <link href="{{ asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}"
          rel="stylesheet">
    <link href="{{ asset('vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
@endsection
<!-- Page /Specific Stylesheets -->


<!-- Page Content -->
@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>View Records</h3>
                </div>

                <div class="title_right">
                    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                            <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Filter Data</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <form method="POST" action="/filterData">
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>NIC <a data-toggle="tooltip"
                                                      title="Use this field to filter out a single person. Leave blank to show all."
                                                      data-placement="right">(?)</a></label>
                                        <input type="text" class="form-control" placeholder="NIC Number" name="NIC">
                                    </div>
                                    <div class="col-md-2">
                                        <label>Age (Between) <a data-toggle="tooltip"
                                                                title="To filter people with the same age, set both dropdowns to the same value."
                                                                data-placement="right">(?)</a></label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <select class="form-control" placeholder="From" name="ageFrom">
                                                    <option value="">From</option>
                                                    @for($i=1;$i<101;$i++)
                                                        <option value="{{ $i }}">{{ $i }}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <select class="form-control" name="ageTo">
                                                    <option value="">To</option>
                                                    @for($i=1;$i<101;$i++)
                                                        <option value="{{ $i }}">{{ $i }}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <label>Gender</label>
                                        <select class="form-control" name="GENDER">
                                            <option value="">Any</option>
                                            <option value="male">Male</option>
                                            <option value="female">Female</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <label>City</label>
                                        <select class="form-control" name="CITY">
                                            <option value="">Any</option>
                                            @foreach($city as $c)
                                                <option value="{{$c->CITY}}">{{$c->CITY}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <label>Designation</label>
                                        <select class="form-control" name="DESIGNATION_CURRENT">
                                            <option value="">Any</option>
                                            @foreach ($designations as $designation)
                                                <option value="{{$designation->DESIGNATION_CURRENT}}">{{$designation->DESIGNATION_CURRENT}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <a class="add-custom-filter float-right text-success"><i
                                                    class="fa fa-plus-circle"></i> Add Custom Filter</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-5">
                                        <label>Column Name <a data-toggle="tooltip"
                                                              title="Select the column to be filtered."
                                                              data-placement="right">(?)</a></label>
                                    </div>
                                    <div class="col-md-5">
                                        <label>Query <a data-toggle="tooltip"
                                                        title="Enter the search query to be filtered from the selected column"
                                                        data-placement="right">(?)</a></label>
                                    </div>
                                    <div class="col-md-2">

                                    </div>
                                </div>
                                <div class="custom-filters">
                                    <div class="row custom-filter">
                                        <div class="col-md-5">
                                            <select class="form-control" name="col-0">
                                                @foreach($colList as $key=>$val)
                                                    @if($val=='ID' || $val=='GENDER' || $val=='AGE' || $val=='NIC' || $val=='CITY' || $val=='DESIGNATION_CURRENT')
                                                        @continue;
                                                    @else
                                                        <option value="{{$val}}">{{$val}}</option>
                                                    @endif
                                                @endforeach
                                                {{--<option value="FULL_NAME">FULL_NAME</option>--}}
                                                {{--<option value="PHONE_OFFICE">PHONE_OFFICE</option>--}}
                                                {{--<option value="PHONE_PERSONAL">PHONE_PERSONAL</option>--}}
                                                {{--<option value="PHONE_MOBILE">PHONE_MOBILE</option>--}}
                                                {{--<option value="EMAIL_OFFICE">EMAIL_OFFICE</option>--}}
                                                {{--<option value="EMAIL_PERSONAL">EMAIL_PERSONAL</option>--}}
                                                {{--<option value="DESIGNATION_PREVIOUS">DESIGNATION_PREVIOUS</option>--}}
                                                {{--<option value="FIRST_APPOINTMENT_DATE">FIRST_APPOINTMENT_DATE</option>--}}
                                                {{--<option value="CURRENT_APPOINTMENT_DATE">CURRENT_APPOINTMENT_DATE--}}
                                                {{--</option>--}}
                                                {{--<option value="BIRTHDAY">BIRTHDAY</option>--}}
                                                {{--<option value="ADDRESS_OFFICE">ADDRESS_OFFICE</option>--}}
                                                {{--<option value="ADDRESS_PERSONAL">ADDRESS_PERSONAL</option>--}}
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" placeholder="Your search query"
                                                   name="where0">
                                        </div>
                                        <div class="col-md-2">
                                            <button class="btn btn-danger disabled" type="reset" style="width: 100%">
                                                DELETE
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button class="btn btn-success float-right" type="submit">FILTER</button>
                                        <button class="btn btn-danger float-right" type="reset">CANCEL</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            {{--Shazeen's--}}
            @if(session()->has('trigger'))
                @if(session()->get('trigger') == 'triggered')
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel text-center"
                                 style="background-color: #f8f8f8;padding-top: 30px; padding-bottom: 30px;">
                                <h5 style="color:#4B5F71;">Please click below button to Send SMS</h5>
                                <a class="btn btn-dark" data-toggle="modal" data-target="#sendSmsSection">SEND
                                    SMS</a>
                            </div>
                            <div class="modal fade" id="sendSmsSection" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <form method="POST" action="/sendSms">
                                        {{csrf_field()}}
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;
                                                </button>
                                                <h4 class="modal-title">SEND SMS</h4>
                                            </div>
                                            <div class="modal-body">

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>IP Address</label>
                                                        <input type="text" name="ip" class="form-control">
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label>Port #</label>
                                                        <input type="text" name="port" class="form-control">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>Username</label>
                                                        <input type="text" name="username" class="form-control">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>Password</label>
                                                        <input type="text" name="password" class="form-control">
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label>Service Provider</label>
                                                        <select name="provider" class="form-control">
                                                            <option value="DIALOG">Dialog</option>
                                                            <option value="MOBITEL">Mobitel</option>
                                                            <option value="ETISALAT">Etisalat</option>
                                                            <option value="HUTCH">Hutch</option>
                                                            <option value="AIRTEL">Airtel</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>Message</label>
                                                        <textarea class="form-control" name="message"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">CLOSE
                                                </button>
                                                <button class="btn btn-success float-right" type="submit">SEND</button>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                            {{--Aviska's--}}
                            {{--<div class="x_panel">--}}
                            {{--<div class="x_title">--}}
                            {{--<h2>Send SMS</h2>--}}
                            {{--<ul class="nav navbar-right panel_toolbox">--}}
                            {{--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>--}}
                            {{--</li>--}}
                            {{--<li class="dropdown">--}}
                            {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"--}}
                            {{--aria-expanded="false"><i class="fa fa-wrench"></i></a>--}}
                            {{--<ul class="dropdown-menu" role="menu">--}}
                            {{--<li><a href="#">Settings 1</a>--}}
                            {{--</li>--}}
                            {{--<li><a href="#">Settings 2</a>--}}
                            {{--</li>--}}
                            {{--</ul>--}}
                            {{--</li>--}}
                            {{--<li><a class="close-link"><i class="fa fa-close"></i></a>--}}
                            {{--</li>--}}
                            {{--</ul>--}}
                            {{--<div class="clearfix"></div>--}}
                            {{--</div>--}}
                            {{--<div class="x_content">--}}
                            {{--<form method="POST" action="/sendSms">--}}
                            {{--{{csrf_field()}}--}}
                            {{--<div class="row">--}}
                            {{--<div class="col-md-2">--}}
                            {{--<label>IP Address</label>--}}
                            {{--<input type="text" name="ip" class="form-control">--}}
                            {{--</div>--}}
                            {{--<div class="col-md-2">--}}
                            {{--<label>Port #</label>--}}
                            {{--<input type="text" name="port" class="form-control">--}}
                            {{--</div>--}}
                            {{--<div class="col-md-2">--}}
                            {{--<label>Username</label>--}}
                            {{--<input type="text" name="username" class="form-control">--}}
                            {{--</div>--}}
                            {{--<div class="col-md-2">--}}
                            {{--<label>Password</label>--}}
                            {{--<input type="text" name="password" class="form-control">--}}
                            {{--</div>--}}
                            {{--<div class="col-md-4">--}}
                            {{--<label>Service Provider</label>--}}
                            {{--<select name="provider" class="form-control">--}}
                            {{--<option value="DIALOG">Dialog</option>--}}
                            {{--<option value="MOBITEL">Mobitel</option>--}}
                            {{--<option value="ETISALAT">Etisalat</option>--}}
                            {{--<option value="HUTCH">Hutch</option>--}}
                            {{--<option value="AIRTEL">Airtel</option>--}}
                            {{--</select>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="clearfix"></div>--}}
                            {{--<div class="row">--}}
                            {{--<div class="col-md-12">--}}
                            {{--<label>Message</label>--}}
                            {{--<textarea class="form-control" name="message"></textarea>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="row">--}}
                            {{--<div class="col-md-12">--}}
                            {{--<button class="btn btn-success float-right" type="submit">SEND</button>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</form>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                        </div>
                    </div>
                @endif
            @endif
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Search Results</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <table id="employee-table" class="table table-striped table-bordered bulk_action">
                                <thead>
                                <tr>
                                    @foreach($colList as $key=>$val)
                                        <th>{{$val}}</th>
                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($employees as $employee)
                                    <tr>
                                        @foreach(json_decode(json_encode($employee),true) as $val)
                                            <td>{{$val}}</td>
                                        @endforeach
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<!-- /Page Content -->

<!-- Page Specific Scripts -->
@section('scripts')
    <!-- New Filter Script -->
    <script>
        var count = 0;
        $(document).ready(function () {
            $(document).on('click', '.add-custom-filter', function () {
                count++;
                $('.custom-filters').append('<div class="row custom-filter" data-transition="pop">\n' +
                    '                                        <div class="col-md-5">\n' +
                    '                                            <select class="form-control" name="col-' + count + '">\n' +
                @foreach($colList as $key=>$val)
                @if($val=='ID' || $val=='GENDER' || $val=='AGE' || $val=='NIC' || $val=='CITY' || $val=='DESIGNATION_CURRENT')
                @continue;
                @else
            '<option value="{{$val}}">{{$val}}</option>'+
                        @endif
                                @endforeach
//                    '                                                <option value="FULL_NAME" selected>FULL_NAME</option>\n' +
//                    '                                                <option value="PHONE_OFFICE">PHONE_OFFICE</option>\n' +
//                    '                                                <option value="PHONE_PERSONAL">PHONE_PERSONAL</option>\n' +
//                    '                                                <option value="PHONE_MOBILE">PHONE_MOBILE</option>\n' +
//                    '                                                <option value="EMAIL_OFFICE">EMAIL_OFFICE</option>\n' +
//                    '                                                <option value="EMAIL_PERSONAL">EMAIL_PERSONAL</option>\n' +
//                    '                                                <option value="DESIGNATION_PREVIOUS">DESIGNATION_PREVIOUS</option>\n' +
//                    '                                                <option value="FIRST_APPOINTMENT_DATE">FIRST_APPOINTMENT_DATE</option>\n' +
//                    '                                                <option value="CURRENT_APPOINTMENT_DATE">CURRENT_APPOINTMENT_DATE</option>\n' +
//                    '                                                <option value="BIRTHDAY">BIRTHDAY</option>\n' +
//                    '                                                <option value="ADDRESS_OFFICE">ADDRESS_OFFICE</option>\n' +
//                    '                                                <option value="ADDRESS_PERSONAL">ADDRESS_PERSONAL</option>\n' +
                    '                                            </select>\n' +
                    '                                        </div>\n' +
                    '                                        <div class="col-md-5">\n' +
                    '                                            <input type="text" class="form-control" placeholder="Your search query" name="where-' + count + '"/>\n' +
                    '                                        </div>\n' +
                    '                                        <div class="col-md-2">\n' +
                    '                                            <button class="btn btn-danger delete-custom-filter" type="reset" style="width: 100%">DELETE</button>\n' +
                    '                                        </div>\n' +
                    '                                    </div>')
            });

            $(document).on('click', '.delete-custom-filter', function () {
                $(this).closest('.custom-filter').remove();
            });
        });
    </script>
    <!-- /New Filter Scripts -->
    <script>
        $(document).ready(function () {
            $(document).on('click', '.btn-delete', function () {
                $('.filter').last().remove()
            })
        });
    </script>


    <!-- Datatables -->
    <script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>
    <script src="{{ asset('vendors/jszip/dist/jszip.min.js') }}"></script>
    <script src="{{ asset('vendors/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('vendors/pdfmake/build/vfs_fonts.js') }}"></script>

    <script>
        $(document).ready(function () {
            $('#employee-table').DataTable();

        });
    </script>
@endsection
<!-- /Page Specific Scripts -->