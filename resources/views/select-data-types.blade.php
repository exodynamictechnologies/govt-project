@extends('layouts.master')

<!-- Page Title -->
@section('title','Home')
<!-- /Page Title -->

<!-- Page Specific Stylesheets -->
@section('stylesheets')

@endsection
<!-- Page /Specific Stylesheets -->


<!-- Page Content -->
@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Import Data (2/2)</h3>
                </div>

                <div class="title_right">
                    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                            <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Select Column Types</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            Select the approrpiate column type for each column from the .CSV file.
                            <br/><br/>
                            <form action="/uploadToDatabase" method="POST">
                                {{csrf_field()}}
                                <input type="hidden" name="fileName" value="{{$fileName}}">
                                <table class="table table-striped">
                                    <tr>
                                        <th>Column Type</th>
                                        <th>Data from File</th>
                                    </tr>
                                    <?php $count = 0; ?>
                                    @foreach($model as $colAndVal)
                                        <tr>
                                            <td>
                                                <select class="form-control" name="{{$count++}}">
                                                    <option value="IGNORE">IGNORE THIS COLUMN</option>
                                                    @foreach($colAndVal->columnList as $col)
                                                        @if($col == 'ID')
                                                            @continue;
                                                        @else
                                                            <option value="{{$col}}">{{$col}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>{{$colAndVal->mappedValue}}</td>
                                        </tr>
                                    @endforeach
                                </table>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button class="btn btn-success float-right" type="submit">SAVE</button>
                                        <button class="btn btn-danger float-right" type="reset">CANCEL</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<!-- /Page Content -->

<!-- Page Specific Scripts -->
@section('scripts')

@endsection
<!-- /Page Specific Scripts -->