@extends('layouts.master')

<!-- Page Title -->
@section('title','Import Data')
<!-- /Page Title -->

<!-- Page Specific Stylesheets -->
@section('stylesheets')
    <!-- Dropzone.js -->
    <link href="{{ asset('vendors/dropzone/dist/min/dropzone.min.css')}}" rel="stylesheet">
@endsection
<!-- Page /Specific Stylesheets -->


<!-- Page Content -->
@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Import Data</h3>
                </div>

                <div class="title_right">
                    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                            <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Upload .CSV File</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <p>Drag .CSV file to the box below or click to select the file. You will be proceeded to the
                                next stage once the upload is complete.</p>
                            {{--<form action="{{url('/uploadFile')}}" class="dropzone" method="post" id="dropzoneJsForm">--}}
                            {{--{{csrf_field()}}--}}
                            {{--</form>--}}
                            <div class="row upload-form">
                                <div class="col-md-6 col-md-offset-3">
                                    <form action="{{url('/uploadFile')}}" method="POST" enctype="multipart/form-data"
                                          class="text-center">
                                        <label>Select .csv file to upload</label>
                                        <input type="file" name="file"
                                               style="margin-left: auto;margin-right: auto;margin-top: 10px;margin-bottom: 10px;">
                                        <button class="btn btn-success" type="submit">Upload</button>
                                    </form>
                                </div>
                            </div>
                            <br/>
                            <br/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<!-- /Page Content -->

<!-- Page Specific Scripts -->
@section('scripts')
    <!-- Dropzone.js -->
    <script src="{{ asset('vendors/dropzone/dist/min/dropzone.min.js') }}"></script>
    <script>
        Dropzone.options.dropzoneJsForm = {
            maxFilesize: 10, // Mb
            init: function () {
                // Set up any event handlers
                this.on('complete', function () {
//                    window.location.href = "/data/import/2";
                });
            }
        };
    </script>
@endsection
<!-- /Page Specific Scripts -->