<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/uploadFile','FileController@uploadFile')->name('upload-file');

Route::get('/','HomeController@index');

Route::get('/data/import','DataController@viewImportData');
Route::get('/data/import/2','FileController@assignFields');
Route::get('/data/view','DataController@viewData');


Route::get('/column/add','ColumnController@viewAddColumn');
Route::get('/column/view','ColumnController@viewColumns');
ROute::post('/addColumn','ColumnController@addColumn');

Route::post('/uploadToDatabase','DataController@uploadToDatabase');
Route::post('/filterData','DataController@filterData');
Route::get('/filterData','DataController@viewData');

Route::post('/sendSms','SmsController@sendSms');