<?php

namespace App\Http\Controllers;

use App\Domain\UIDataMapper;
use App\Services\AssignToMapper;
use App\Services\ColumnIdentifier;
use App\Services\FileUploader;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class FileController extends Controller
{
    public function uploadFile(Request $request)
    {
        Log::info("uploadFile controller hit");
        $fileUploader = new FileUploader();
        $columnIdentifier = new ColumnIdentifier();
        $uploaded_file = $request->file;
        $path = $fileUploader->uploadFileToServer($uploaded_file);
        $values = $columnIdentifier->splitValuesIntoFields($path);
        Log::info('values retrieved -- '.json_encode($values));
        Log::info("values assigned to variable");
        $mapper = new AssignToMapper();
        $model = $mapper->assignValuesToMapperModel($values);
        $splitFilePath = explode("/",$path);
        $fileName = $splitFilePath[sizeof($splitFilePath)-1];
        return view('select-data-types',['model' => $model,'fileName'=>$fileName]);
    }
}
