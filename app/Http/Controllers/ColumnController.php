<?php

namespace App\Http\Controllers;

use App\Services\ColumnService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;


class ColumnController extends Controller
{
	public function viewAddColumn(Request $request)
	{
	    Log::info("Add Column Page");
		return view('add-column');
	}

	public function viewColumns(Request $request)
	{
		return view('view-columns');
	}

	public function addColumn(Request $request){
	    $colName = $request->colName;
	    $colName = str_replace(' ','_',$colName);
        Log::info('request received with column name '.$colName);
	    $columnService = new ColumnService();
	    $columnService->createColumn($colName);
    }
}
