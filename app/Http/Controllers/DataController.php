<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Services\ColumnIdentifier;
use App\Services\FilterIdentifier;
use App\Services\UploadData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class DataController extends Controller
{
    public function viewImportData(Request $request)
    {
        return view('import-data');
    }

    public function viewSelectDataTypes(Request $request)
    {
        return view('select-data-types');
    }

    public function viewData(Request $request)
    {
        session()->forget('trigger');
        $employees = Employee::all();
        $designations = DB::table('info_employee')->select('DESIGNATION_CURRENT')->groupBy('DESIGNATION_CURRENT')->get();
        $city = DB::table('info_employee')->select('CITY')->groupBy('CITY')->get();
        $colList = DB::getSchemaBuilder()->getColumnListing('info_employee');
        return view('view-data', ['employees' => $employees, 'designations' => $designations, 'city' => $city, 'colList' => $colList]);
    }

    public function uploadToDatabase(Request $request)
    {
        $columnList = $request->except(['_token', 'fileName']);
        $fileName = $request->fileName;
        $uploadData = new UploadData();
        $uploadData->uploadDataToDatabase($columnList, $fileName);
        return redirect()->action('DataController@viewData');
    }

    public function filterData(Request $request)
    {
        $filters = $request->except(['_token']);
        $filterIdentifier = new FilterIdentifier();
        $allFilters = $filterIdentifier->addFiltersToArray($filters);
        Log::info('FINAL FILTER SET - ' . json_encode($allFilters));
        $employees = $filterIdentifier->queryDB($allFilters);
        session(['employees' => $employees, 'trigger' => 'triggered']);
        $designations = DB::table('info_employee')->select('DESIGNATION_CURRENT')->groupBy('DESIGNATION_CURRENT')->get();
        $city = DB::table('info_employee')->select('CITY')->groupBy('CITY')->get();
        $colList = DB::getSchemaBuilder()->getColumnListing('info_employee');
        return view('view-data', ['employees' => $employees, 'designations' => $designations, 'city' => $city, 'colList' => $colList]);
    }
}
