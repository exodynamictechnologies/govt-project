<?php

namespace App\Http\Controllers;

use App\Services\SmsProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SmsController extends Controller
{
    public function sendSms(Request $request)
    {
        $provider = $request->provider;
        $ip = $request->ip;
        $port = $request->port;
        $username = $request->username;
        $password = $request->password;
        $message = $request->message;
        Log::info($provider.$ip.$port.$username.$password.$message);
        Log::info("Filtered data from session - ".json_encode(session('employees')));
        $smsProvider = new SmsProvider();
        $result = $smsProvider->selectProvider($provider, session('employees'), $ip, $port, $username, $password, $message);
        var_dump($result);
    }
}
