<?php
/**
 * Created by PhpStorm.
 * User: Avishka-Perera
 * Date: 12/9/2017
 * Time: 11:10 AM
 */

namespace App\Services;


use App\Employee;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UploadData
{
    public function uploadDataToDatabase($columnList, $fileName)
    {
        $filePath = base_path('/storage/app/public/documents/' . $fileName);
        $file = fopen($filePath, "r");
        $query = "INSERT INTO info_employee (";
        $countArr = array();
        $count = 0;
        foreach ($columnList as $column) {
            if($column == 'IGNORE'){
                $count++;
                array_push($countArr,$count);
            }else{
                $count++;
                $query .=$column.",";
            }
        }
        Log::info('CountArr values to ignore = '.json_encode($countArr));
        $query = rtrim($query, ',');
        $query .= ") VALUES (";
        $counter = 0;
        while (($data = fgetcsv($file)) != false) {
            $counter++;
            if($counter > 1){
//                $values = explode(',',$data);
                $countValues = 0;
                foreach($data as $value){
                    $countValues++;
                    if(in_array($countValues,$countArr)){
                        Log::info('Count ignored from values '.$countValues);
                        continue;
                    }else{
//                        $query.="'".str_replace(';',',',$value)."',";
                        $query.="\"".$value."\",";
                    }
                }
                $query = rtrim($query,',');
                $query.="),(";
            }
        }
        $query = rtrim($query,',(');
        Log::info("\n\nUploadData -- query -- ".$query);
        DB::statement($query);
    }
}