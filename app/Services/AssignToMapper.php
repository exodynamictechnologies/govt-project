<?php
/**
 * Created by PhpStorm.
 * User: Avishka-Perera
 * Date: 12/9/2017
 * Time: 8:43 AM
 */

namespace App\Services;


use App\Domain\UIDataMapper;
use Illuminate\Support\Facades\Log;

class AssignToMapper
{
    public function assignValuesToMapperModel($values){
        Log::info('AssignToMapper -- values received -- '.json_encode($values));
        $modelArray = array();
        foreach($values as $value){
            $model = new UIDataMapper();
            $model->mappedValue = str_replace(';',',',$value);
            Log::info('AssignToMapper -- model -- '.json_encode($model));
            array_push($modelArray,$model);
            Log::info("created model -- ".json_encode($modelArray));
        }
        return $modelArray;
    }
}