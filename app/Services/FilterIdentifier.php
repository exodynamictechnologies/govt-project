<?php
/**
 * Created by PhpStorm.
 * User: Avishka-Perera
 * Date: 12/9/2017
 * Time: 2:14 PM
 */

namespace App\Services;


use App\Domain\FilterModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class FilterIdentifier
{
    public function addFiltersToArray($filters)
    {
//        $keys = array_keys($filters);
//        Log::info(json_encode($keys));
//        $singleFilter = array();
//        $allFilters = array();
//        $count = 0;
////        for ($i = 0; $i < sizeof($filters); $i++) {
////            $count++;
////            if ($count > 2) {
////                array_push($allFilters, $singleFilter);
////                Log::info('FilterIdentifier -- inside if statement -- ' . json_encode($allFilters));
////                $singleFilter = array();
////                $count = 0;
////            }
////            array_push($singleFilter, $filters[$keys[$i]]);
////            Log::info('FilterIdentifier -- ' . json_encode($singleFilter));
////        }
////        array_push($allFilters, $singleFilter);
////        Log::info('FilterIdentifier -- ' . json_encode($allFilters));
////        return $allFilters;
//        for ($i = 0; $i < sizeof($keys); $i++) {
//            array_push($singleFilter, $keys[$i]);
//            array_push($singleFilter, 'LIKE');
//            array_push($singleFilter, '% '.$filters[$keys[$i]].' %');
//            array_push($allFilters, $singleFilter);
//            $singleFilter = array();
//            Log::info('FilterIdentifier -- ' . json_encode($singleFilter));
//        }
//        Log::info('FilterIdentifier -- ' . json_encode($allFilters));
//        return $allFilters;$
        $filterModel = new FilterModel();
        $filterModel->nic = $filters['NIC'];
        $filterModel->ageFrom = $filters['ageFrom'];
        $filterModel->ageTo = $filters['ageTo'];
        $filterModel->gender = $filters['GENDER'];
        $filterModel->city = $filters['CITY'];
        $filterModel->designation = $filters['DESIGNATION_CURRENT'];
        foreach($filters as $key=>$value){
            if(count(explode('col',$key)) > 1){
                array_push($filterModel->customCol,$value);
                Log::info('custom column name - '.$value);
            }
            if(count(explode('where',$key)) > 1){
                array_push($filterModel->customVal,$value);
                Log::info('custom value - '.$value);
            }
        }
        return $filterModel->build();
    }

    public function queryDB($allFilters)
    {
        if(is_array($allFilters)){
            $employees = DB::table('info_employee')->where($allFilters)->get();
        }else{
            $employees = DB::table('info_employee')->get();
        }
        Log::info('FilterIdentifier -- queried data -- ' . json_encode($employees));
        return $employees;
    }
}