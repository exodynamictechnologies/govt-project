<?php
/**
 * Created by PhpStorm.
 * User: Avishka-Perera
 * Date: 12/2/2017
 * Time: 5:21 PM
 */

namespace App\Services;


use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class ColumnIdentifier
{
    public function splitValuesIntoFields($path){
        $splitFilePath = explode("/",$path);
        $fileName = $splitFilePath[sizeof($splitFilePath)-1];
        Log::info('File Name -- '.$fileName);
        $filePath = base_path('/storage/app/public/documents/'.$fileName);
        Log::info('Full file path of file -- '.$filePath);
        $file = fopen($filePath,"r");
        $values = array();
        $count = 0;
//        while(($data = fgets($file)) != false){
//            $count++;
//            if($count > 5){
//                Log::info('CONTENT -- '.$data);
//                Log::info('ENCODE TYPE ---- '.mb_detect_encoding($data));
//                $values = explode(',',$data);
//                break;
//            }
//        }
        while(($data = fgetcsv($file)) != false){
            $count++;
            if($count > 5){
                Log::info('CONTENT -- '.json_encode($data));
//                Log::info('ENCODE TYPE ---- '.mb_detect_encoding($data));
//                $values = explode(',',$data);
                $values = $data;
                break;
            }
        }
        fclose($file);
        return $values;
    }

    public function getColumnValuesOnly($colAndVal){
        $colValues = array();
        for($col=0;$col<sizeof($colAndVal)-1;$col++){
            array_push($colValues,$colAndVal[''.$col]);
            Log::info('ColumnIdentifier -- values only -- '.$colAndVal[''.$col]);
        }
        return $colValues;
    }
}