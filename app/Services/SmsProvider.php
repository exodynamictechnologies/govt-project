<?php
/**
 * Created by PhpStorm.
 * User: Avishka-Perera
 * Date: 12/18/2017
 * Time: 9:44 PM
 */

namespace App\Services;


use Illuminate\Support\Facades\Log;

class SmsProvider
{
    private $smsSender;

    public function selectProvider($provider, $employees, $ip, $port, $username, $password, $message)
    {
        if ($provider == 'DIALOG') {
            $dialogUsers = $this->filterDialog($employees);
            Log::info("Dialog users - ".json_encode($dialogUsers));
            return $this->sendUsingDialog($dialogUsers, $ip, $port, $username, $password, $message);
        } else if ($provider == 'MOBITEL') {
            $mobitelUsers = $this->filterMobitel($employees);
            return $this->sendUsingMobitel($mobitelUsers, $ip, $port, $username, $password, $message);
        } else if ($provider == 'HUTCH') {
            $hutchUsers = $this->filterHutch($employees);
            return $this->sendUsingHutch($hutchUsers, $ip, $port, $username, $password, $message);
        } else if ($provider == 'ETISALAT') {
            $etisalatUsers = $this->filterEtisalat($employees);
            return $this->sendUsingEtisalat($etisalatUsers, $ip, $port, $username, $password, $message);
        } else if ($provider == 'AIRTEL') {
            $airtelUsers = $this->filterAirtel($employees);
            return $this->sendUsingAirtel($airtelUsers, $ip, $port, $username, $password, $message);
        }
    }

    private function sendUsingDialog($dialogUsers, $ip, $port, $username, $password, $message)
    {
        $this->smsSender = new SmsSender();
        return $this->smsSender->sendSms($ip, $port, $username, $password, $dialogUsers, $message);
    }

    private function sendUsingMobitel($mobitelUsers, $ip, $port, $username, $password, $message)
    {
        $this->smsSender = new SmsSender();
        return $this->smsSender->sendSms($ip, $port, $username, $password, $mobitelUsers, $message);
    }

    private function sendUsingHutch($hutchUsers, $ip, $port, $username, $password, $message)
    {
        $this->smsSender = new SmsSender();
        return $this->smsSender->sendSms($ip, $port, $username, $password, $hutchUsers, $message);
    }

    private function sendUsingEtisalat($etisalatUsers, $ip, $port, $username, $password, $message)
    {
        $this->smsSender = new SmsSender();
        return $this->smsSender->sendSms($ip, $port, $username, $password, $etisalatUsers, $message);
    }

    private function sendUsingAirtel($airtelUsers, $ip, $port, $username, $password, $message)
    {
        $this->smsSender = new SmsSender();
        return $this->smsSender->sendSms($ip, $port, $username, $password, $airtelUsers, $message);
    }

    private function filterDialog($employees)
    {
        $dialogUsers = array();
        foreach ($employees as $employee) {
            $employee->PHONE_MOBILE = $this->stripHyphen($employee->PHONE_MOBILE);
            Log::info("Filter strip hyphen - ".$employee->PHONE_MOBILE);
            $employee->PHONE_MOBILE = $this->stripLeadingZero($employee->PHONE_MOBILE);
            Log::info("Strip leading zero - ".$employee->PHONE_MOBILE);
            if ($this->validateDialogNumber($employee->PHONE_MOBILE)) {
                array_push($dialogUsers, $employee);
            }
        }
        return $dialogUsers;
    }

    private function validateDialogNumber($number)
    {
        $code = substr($number, 0, 2);
        if ($code == '77' || $code == '76')
            return true;
        else
            return false;
    }

    private function filterMobitel($employees)
    {
        $mobitelUsers = array();
        foreach ($employees as $employee) {
            $employee->PHONE_MOBILE = $this->stripHyphen($employee->PHONE_MOBILE);
            $employee->PHONE_MOBILE = $this->stripLeadingZero($employee->PHONE_MOBILE);
            Log::info($employee->PHONE_MOBILE);
            if ($this->validateMobitelNumber($employee->PHONE_MOBILE)) {
                array_push($mobitelUsers, $employee);
            }
        }
        return $mobitelUsers;
    }

    private function validateMobitelNumber($number)
    {
        $code = substr($number, 0, 2);
        if ($code == '71')
            return true;
        else
            return false;
    }

    private function filterHutch($employees)
    {
        $hutchUsers = array();
        foreach ($employees as $employee) {
            $employee->PHONE_MOBILE = $this->stripHyphen($employee->PHONE_MOBILE);
            $employee->PHONE_MOBILE = $this->stripLeadingZero($employee->PHONE_MOBILE);
            Log::info($employee->PHONE_MOBILE);
            if ($this->validateHutchNumber($employee->PHONE_MOBILE)) {
                array_push($hutchUsers, $employee);
            }
        }
        return $hutchUsers;
    }

    private function validateHutchNumber($number)
    {
        $code = substr($number, 0, 2);
        if ($code == '78')
            return true;
        else
            return false;
    }

    private function filterEtisalat($employees)
    {
        $etisalatUsers = array();
        foreach ($employees as $employee) {
            $employee->PHONE_MOBILE = $this->stripHyphen($employee->PHONE_MOBILE);
            $employee->PHONE_MOBILE = $this->stripLeadingZero($employee->PHONE_MOBILE);
            Log::info($employee->PHONE_MOBILE);
            if ($this->validateEtisalatNumber($employee->PHONE_MOBILE)) {
                array_push($etisalatUsers, $employee);
            }
        }
        return $etisalatUsers;
    }

    private function validateEtisalatNumber($number)
    {
        $code = substr($number, 0, 2);
        if ($code == '72')
            return true;
        else
            return false;
    }

    private function filterAirtel($employees)
    {
        $airtelUsers = array();
        foreach ($employees as $employee) {
            $employee->PHONE_MOBILE = $this->stripHyphen($employee->PHONE_MOBILE);
            $employee->PHONE_MOBILE = $this->stripLeadingZero($employee->PHONE_MOBILE);
            Log::info($employee->PHONE_MOBILE);
            if ($this->validateAirtelNumber($employee->PHONE_MOBILE)) {
                array_push($airtelUsers, $employee);
            }
        }
        return $airtelUsers;
    }

    private function validateAirtelNumber($number)
    {
        $code = substr($number, 0, 2);
        if ($code == '75')
            return true;
        else
            return false;
    }

    private function stripLeadingZero($number)
    {
        if (substr($number, 0, 1) == '0') {
            return ltrim($number, '0');
        }
        return $number;
    }

    private function stripHyphen($number)
    {
        return str_replace('-', '', $number);
    }
}