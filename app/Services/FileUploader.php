<?php
/**
 * Created by PhpStorm.
 * User: Avishka-Perera
 * Date: 12/2/2017
 * Time: 5:27 PM
 */

namespace App\Services;


use Illuminate\Http\File;
use Illuminate\Support\Facades\Log;

class FileUploader
{
    public function uploadFileToServer($file){
        $path = $file->store('public/documents');
        Log::info("file stored to ".$path);
        return $path;
    }
}