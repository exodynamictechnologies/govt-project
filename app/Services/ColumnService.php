<?php
/**
 * Created by PhpStorm.
 * User: Avishka-Perera
 * Date: 1/6/2018
 * Time: 11:15 AM
 */

namespace App\Services;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ColumnService
{
    public function createColumn($colName){
        $query = 'ALTER TABLE info_employee ADD COLUMN '.$colName.' VARCHAR(255)';
        Log::info("Query to alter table -- ".$query);
        DB::statement($query);
    }
}