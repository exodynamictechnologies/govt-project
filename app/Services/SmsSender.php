<?php
/**
 * Created by PhpStorm.
 * User: Avishka-Perera
 * Date: 12/19/2017
 * Time: 8:40 PM
 */

namespace App\Services;


use Illuminate\Support\Facades\Log;

class SmsSender
{
    public function sendSms($ip,$port,$username,$password,$employees,$message){
//        $socket = socket_create(AF_INET,SOCK_STREAM,SOL_TCP);
//        if($socket === false){
//            Log::info("Socket error - "+socket_strerror(socket_last_error($socket)));
//            return;
//        }
//        $smsSocket = socket_connect($socket,$ip,$port);
//        if($smsSocket === false){
//            Log::info("SMS socket error - "+socket_strerror(socket_last_error($socket)));
//        }
        $response = '';
        $success = 0;
        $failed = 0;
        $result = array();
        foreach ($employees as $employee){
            $socket = socket_create(AF_INET,SOCK_STREAM,SOL_TCP);
            if($socket === false){
                Log::info("Socket error - "+socket_strerror(socket_last_error($socket)));
                return;
            }
            $smsSocket = socket_connect($socket,$ip,$port);
            if($smsSocket === false){
                Log::info("SMS socket error - "+socket_strerror(socket_last_error($socket)));
            }
            $query = '/http/send-message/';
            $query .= '?username='.urlencode($username);
            $query .= '&password='.urlencode($password);
            $query .= '&to='.urlencode($employee->PHONE_MOBILE);
            $query .= '&message='.urlencode($message);
            Log::info("SMS server query - ".$query);

            $in = "GET ".$query." HTTP/1.1\r\n";
            $in .= "Host: www.myhost.com\r\n";
            $in .= "Connection: Close\r\n\r\n";
            Log::info("Sending string - ".$in);

            socket_write($socket,$in,strlen($in));

            $out = '';
            while($buffer = socket_read($socket,2048)){
                $out = $out.$buffer;
            }
            $response = explode('\n',$out);
            if(strpos(end($response),'200 OK') !== false){
                $success = $success + 1;
                $result['success'] = $success;
            }   else{
                $failed = $failed + 1;
                $result['failed'] = $failed;
            }
            Log::info("Response from sms server - ".end($response));
            socket_close($socket);
        }
        return $result;
//        socket_close($socket);
    }
}