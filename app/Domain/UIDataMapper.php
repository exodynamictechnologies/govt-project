<?php
/**
 * Created by PhpStorm.
 * User: Avishka-Perera
 * Date: 12/9/2017
 * Time: 8:36 AM
 */

namespace App\Domain;


use Illuminate\Support\Facades\DB;

class UIDataMapper
{
    public $columnList;
    public $mappedValue;

    public function __construct()
    {
//        $this->columnList = array(
//            'FULL_NAME',
//            'NIC',
//            'PHONE_OFFICE',
//            'PHONE_PERSONAL',
//            'PHONE_MOBILE',
//            'EMAIL_OFFICE',
//            'EMAIL_PERSONAL',
//            'DESIGNATION_CURRENT',
//            'DESIGNATION_PREVIOUS',
//            'FIRST_APPOINTMENT_DATE',
//            'CURRENT_APPOINTMENT_DATE',
//            'BIRTHDAY',
//            'ADDRESS_OFFICE',
//            'ADDRESS_PERSONAL',
//            'GENDER'
//        );
        $this->columnList = DB::getSchemaBuilder()->getColumnListing('info_employee');
    }
}