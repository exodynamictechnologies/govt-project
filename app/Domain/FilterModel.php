<?php
/**
 * Created by PhpStorm.
 * User: Avishka-Perera
 * Date: 12/16/2017
 * Time: 1:09 PM
 */

namespace App\Domain;


class FilterModel
{
    public $nic;
    public $ageFrom;
    public $ageTo;
    public $gender;
    public $city;
    public $designation;
    public $customCol = array();
    public $customVal = array();

    private $query = 'SELECT * FROM info_employee';
    private $singleFilter = array();
    private $globalFilter = array();

    public function build()
    {
        if ($this->nic == null &&
            $this->ageFrom == '' &&
            $this->ageTo == '' &&
            $this->gender == '' &&
            $this->city == '' &&
            $this->designation == '' &&
            count($this->customVal) <= 0 &&
            count($this->customCol) <= 0) {
            return $this->query;
        }

        if ($this->nic != null) {
            array_push($this->singleFilter, 'NIC');
            array_push($this->singleFilter, '=');
            array_push($this->singleFilter, $this->nic);
            array_push($this->globalFilter, $this->singleFilter);
            $this->singleFilter = array();
        }

        if ($this->ageFrom != null && $this->ageTo != null) {
            array_push($this->singleFilter, 'AGE');
            array_push($this->singleFilter, '>=');
            array_push($this->singleFilter, $this->ageFrom);
            array_push($this->globalFilter, $this->singleFilter);
            $this->singleFilter = array();

            array_push($this->singleFilter, 'AGE');
            array_push($this->singleFilter, '<=');
            array_push($this->singleFilter, $this->ageTo);
            array_push($this->globalFilter, $this->singleFilter);
            $this->singleFilter = array();
        }

        if ($this->gender != null) {
            array_push($this->singleFilter, 'GENDER');
            array_push($this->singleFilter, '=');
            array_push($this->singleFilter, $this->gender);
            array_push($this->globalFilter, $this->singleFilter);
            $this->singleFilter = array();
        }

        if ($this->city != null) {
            array_push($this->singleFilter, 'CITY');
            array_push($this->singleFilter, '=');
            array_push($this->singleFilter, $this->city);
            array_push($this->globalFilter, $this->singleFilter);
            $this->singleFilter = array();
        }

        if ($this->designation != null) {
            array_push($this->singleFilter, 'DESIGNATION_CURRENT');
            array_push($this->singleFilter, '=');
            array_push($this->singleFilter, $this->designation);
            array_push($this->globalFilter, $this->singleFilter);
            $this->singleFilter = array();
        }

        if (count($this->customCol) > 0 && count($this->customVal) > 0) {
            for ($i = 0; $i < count($this->customCol); $i++) {
                array_push($this->singleFilter, $this->customCol[$i]);
                array_push($this->singleFilter, 'like');
                array_push($this->singleFilter, '%'.$this->customVal[$i].'%');
                array_push($this->globalFilter, $this->singleFilter);
                $this->singleFilter = array();
            }
        }
        return $this->globalFilter;
    }
}